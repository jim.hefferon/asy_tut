# Asymptote tutorial by Jim Hefferon

This is a simple introduction to [Asymptote](https://asymptote.sourceforge.io/), a language for making mathematical drawings that integrates with LaTeX.

## License
CC-BY-SA

## Project status
2024-Mar-12 Jim Hefferon  Initial