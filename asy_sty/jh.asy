// jh.asy
// Asymptote common definition file for Asymptote tutorial by Jim Hefferon
void settexpreamble() {
  texpreamble("\usepackage{../asy_sty/asy_tut_asy}");
}

import fontsize;
defaultpen(linewidth(0.5pt)+fontsize(9pt));
